/*
 * Copyright © 2020 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __KMD_H__
#define __KMD_H__

#include <stdint.h>

/**
 * This is the interface to the kernel-mode-driver, it would take a different
 * implementation for kgsl vs drm/msm
 */

struct kmd_device;
struct kmd_bo;

/**
 * Open kernel device
 */
struct kmd_device *kmd_dev_open(void);

/**
 * Submit cmdstream to the kernel, returning a fence/timestamp value.
 */
uint32_t kmd_dev_submit(struct kmd_device *dev, struct kmd_bo *cmds,
		uint32_t sizedwords);

/**
 * Wait for fence.
 */
void kmd_dev_waitfence(struct kmd_device *dev, uint32_t fence);

/**
 * Allocate a GPU buffer object with specified size (in bytes).
 */
struct kmd_bo *kmd_bo_alloc(struct kmd_device *dev, int size);

/**
 * Map the specified buffer for CPU access.  The buffer should be
 * mapped uncached/writecombine.
 */
void *kmd_bo_map(struct kmd_bo *bo);

/**
 * Get the buffers GPU iova
 */
uint64_t kmd_bo_iova(struct kmd_bo *bo);

#endif /* __KMD_H__ */
