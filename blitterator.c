/*
 * Copyright © 2018 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <linux/time.h>

#include "config.h"
#include "kmd.h"
#include "ring.h"

#include "adreno_common.xml.h"
#include "adreno_pm4.xml.h"
#include "a6xx.xml.h"

static struct kmd_device *dev;
static struct kmd_bo *buffers[2 * NUM_BLITS];
static struct kmd_bo *control;
static struct fd_ringbuffer *ring;
static unsigned seqno;

static void
setup_buffers(void)
{
	for (int i = 0; i < ARRAY_SIZE(buffers); i++) {
		buffers[i] = kmd_bo_alloc(dev, BLIT_WIDTH * BLIT_HEIGHT * BLIT_CPP);
	}
	control = kmd_bo_alloc(dev, 0x1000);
}

static void
emit_blit(struct fd_ringbuffer *ring, unsigned n)
{
	struct kmd_bo *src = buffers[(2 * n) + 0];
	struct kmd_bo *dst = buffers[(2 * n) + 1];

	OUT_PKT7(ring, CP_SET_MARKER, 1);
	OUT_RING(ring, A6XX_CP_SET_MARKER_0_MODE(RM6_BLIT2DSCALE));

	OUT_PKT4(ring, REG_A6XX_RB_2D_BLIT_CNTL, 1);
	OUT_RING(ring, A6XX_RB_2D_BLIT_CNTL_MASK(0xf) |
			A6XX_RB_2D_BLIT_CNTL_COLOR_FORMAT(BLIT_FMT) |
			A6XX_RB_2D_BLIT_CNTL_IFMT(BLIT_IFMT));

	OUT_PKT4(ring, REG_A6XX_GRAS_2D_BLIT_CNTL, 1);
	OUT_RING(ring, A6XX_GRAS_2D_BLIT_CNTL_MASK(0xf) |
			A6XX_GRAS_2D_BLIT_CNTL_COLOR_FORMAT(BLIT_FMT) |
			A6XX_GRAS_2D_BLIT_CNTL_IFMT(BLIT_IFMT));

	/*
	 * Emit source:
	 */

	OUT_PKT4(ring, REG_A6XX_SP_PS_2D_SRC_INFO, 10);
	OUT_RING(ring, A6XX_SP_PS_2D_SRC_INFO_COLOR_FORMAT(BLIT_FMT) |
			A6XX_SP_PS_2D_SRC_INFO_TILE_MODE(BLIT_TILE_MODE) |
			A6XX_SP_PS_2D_SRC_INFO_COLOR_SWAP(WZYX) |
			A6XX_SP_PS_2D_SRC_INFO_SAMPLES(MSAA_ONE) |
			0x500000);
	OUT_RING(ring, A6XX_SP_PS_2D_SRC_SIZE_WIDTH(BLIT_WIDTH) |
			 A6XX_SP_PS_2D_SRC_SIZE_HEIGHT(BLIT_HEIGHT)); /* SP_PS_2D_SRC_SIZE */
	OUT_RELOC(ring, src);    /* SP_PS_2D_SRC_LO/HI */
	OUT_RING(ring, A6XX_SP_PS_2D_SRC_PITCH_PITCH(BLIT_WIDTH * BLIT_CPP));
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);

	/*
	 * Emit destination:
	 */
	OUT_PKT4(ring, REG_A6XX_RB_2D_DST_INFO, 9);
	OUT_RING(ring, A6XX_RB_2D_DST_INFO_COLOR_FORMAT(BLIT_FMT) |
			 A6XX_RB_2D_DST_INFO_TILE_MODE(BLIT_TILE_MODE) |
			 A6XX_RB_2D_DST_INFO_COLOR_SWAP(WZYX));
	OUT_RELOC(ring, dst);    /* RB_2D_DST_LO/HI */
	OUT_RING(ring, A6XX_RB_2D_DST_SIZE_PITCH(BLIT_WIDTH * BLIT_CPP));
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);
	OUT_RING(ring, 0x00000000);

	/*
	 * Blit command:
	 */
	OUT_PKT4(ring, REG_A6XX_GRAS_2D_SRC_TL_X, 4);
	OUT_RING(ring, A6XX_GRAS_2D_SRC_TL_X_X(0));
	OUT_RING(ring, A6XX_GRAS_2D_SRC_BR_X_X(BLIT_WIDTH - 1));
	OUT_RING(ring, A6XX_GRAS_2D_SRC_TL_Y_Y(0));
	OUT_RING(ring, A6XX_GRAS_2D_SRC_BR_Y_Y(BLIT_HEIGHT - 1));

	OUT_PKT4(ring, REG_A6XX_GRAS_2D_DST_TL, 2);
	OUT_RING(ring, A6XX_GRAS_2D_DST_TL_X(0) | A6XX_GRAS_2D_DST_TL_Y(0));
	OUT_RING(ring, A6XX_GRAS_2D_DST_BR_X(BLIT_WIDTH - 1) |
			A6XX_GRAS_2D_DST_BR_Y(BLIT_HEIGHT - 1));

	OUT_PKT7(ring, CP_EVENT_WRITE, 1);
	OUT_RING(ring, CP_EVENT_WRITE_0_EVENT(0x3f));

	OUT_PKT4(ring, REG_A6XX_RB_UNKNOWN_8C01, 1);
	OUT_RING(ring, 0);

	OUT_PKT4(ring, REG_A6XX_SP_2D_SRC_FORMAT, 1);
	OUT_RING(ring, A6XX_SP_2D_SRC_FORMAT_COLOR_FORMAT(BLIT_FMT) |
			A6XX_SP_2D_SRC_FORMAT_NORM |
			A6XX_SP_2D_SRC_FORMAT_MASK(0xf));

#if GPU_ID == 630
	OUT_WFI(ring);

	OUT_PKT4(ring, REG_A6XX_RB_UNKNOWN_8E04, 1);
	OUT_RING(ring, 0x01000000);
#endif

	OUT_PKT7(ring, CP_BLIT, 1);
	OUT_RING(ring, CP_BLIT_0_OP(BLIT_OP_SCALE));

#if GPU_ID == 630
	OUT_WFI(ring);

	OUT_PKT4(ring, REG_A6XX_RB_UNKNOWN_8E04, 1);
	OUT_RING(ring, 0x01000000);
#endif
}

static void
emit_finish(struct fd_ringbuffer *ring)
{
	OUT_PKT7(ring, CP_EVENT_WRITE, 4);
	OUT_RING(ring, CP_EVENT_WRITE_0_EVENT(UNK_1D));
	OUT_RELOC(ring, control);  /* ADDR_LO/HI */
	OUT_RING(ring, ++seqno);

	OUT_PKT7(ring, CP_EVENT_WRITE, 4);
	OUT_RING(ring, CP_EVENT_WRITE_0_EVENT(FACENESS_FLUSH));
	OUT_RELOC(ring, control);  /* ADDR_LO/HI */
	OUT_RING(ring, ++seqno);

	OUT_PKT7(ring, CP_EVENT_WRITE, 4);
	OUT_RING(ring, CP_EVENT_WRITE_0_EVENT(CACHE_FLUSH_TS));
	OUT_RELOC(ring, control);  /* ADDR_LO/HI */
	OUT_RING(ring, ++seqno);

	OUT_PKT7(ring, CP_EVENT_WRITE, 1);
	OUT_RING(ring, CP_EVENT_WRITE_0_EVENT(CACHE_INVALIDATE));
}

#define NSEC_PER_SEC 1000000000ull

static uint64_t
gettime_ns(void)
{
	struct timespec current;
	clock_gettime(CLOCK_MONOTONIC, &current);
	return (uint64_t)current.tv_sec * NSEC_PER_SEC + current.tv_nsec;
}

int
main(int argc, char **argv)
{
	uint64_t start, stop, bytes;

	dev = kmd_dev_open();
	ring = fd_ringbuffer_new(dev, 0x40000);

	setup_buffers();

	for (int i = 0; i < NUM_ITERATIONS; i++) {
		for (int n = 0; n < NUM_BLITS; n++) {
			emit_blit(ring, n);
			emit_finish(ring);
		}
	}

	/* Make sure our buffers are pinned in the kernel and the GPU
	 * is powered on before we start timing.  Note that we create
	 * the submit ioctl in kmd_drm.c as we go, so you can't make
	 * more than one ringbuffer to submit!
	 */
	fd_ringbuffer_flush(ring);

	start = gettime_ns();
	fd_ringbuffer_flush(ring);
	stop = gettime_ns();

	/*
	 * Total number of blits is NUM_ITERATIONS * NUM_BLITS, with each
	 * blit copying BLIT_WIDTH * BLIT_HEIGHT * BLIT_CPP:
	 */
	bytes = NUM_ITERATIONS * NUM_BLITS * BLIT_WIDTH * BLIT_HEIGHT * BLIT_CPP;
	printf("%"PRIu64" bytes copied in %"PRIu64"ns\n", bytes, stop - start);

	double gb = (double)bytes / (1024.0 * 1024.0 * 1024.0);
	double sec = (double)(stop - start) / (double)NSEC_PER_SEC;

	printf(" => %f GB/sec\n", gb / sec);
}
