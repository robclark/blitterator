LOCAL_PATH := $(call my-dir)

#
# Build libwrap:
#

include $(CLEAR_VARS)
LOCAL_MODULE	:= blitterator
LOCAL_SRC_FILES	:= blitterator.c kmd_kgsl.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)/includes $(LOCAL_PATH)/util
LOCAL_LDLIBS := -llog -lc -ldl
include $(BUILD_EXECUTABLE)

