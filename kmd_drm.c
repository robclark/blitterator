/*
 * Copyright © 2020 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <linux/time.h>
#include <fcntl.h>
#include <unistd.h>

#include <xf86drm.h>

#include "config.h"
#include "kmd.h"
#include "msm_drm.h"

/**
 * This is the implementation of the kernel-mode-driver for drm/msm.
 */

struct kmd_device {
	int fd;
	uint32_t queue_id;

	uint32_t nr_bos;
	struct drm_msm_gem_submit_bo bos[(2 * NUM_BLITS) + 5];
};

struct kmd_bo {
	struct kmd_device *dev;
	uint64_t iova;
	uint32_t handle;
	uint32_t size;
};

static int
try_open_device(const char *path)
{
	drmVersionPtr version;
	int fd;

	fd = open(path, O_RDWR);
	if (fd < 0)
		return fd;

	version = drmGetVersion(fd);
	if (!version)
		return -1;

	if (strcmp(version->name, "msm") != 0) {
		close(fd);
		fd = -1;
	}

	drmFreeVersion(version);

	return fd;
}

static uint32_t
open_submitqueue(struct kmd_device *dev)
{
	struct drm_msm_submitqueue req = {
		.flags = 0,
		.prio = 0,
	};
	int ret;

	ret = drmCommandWriteRead(dev->fd, DRM_MSM_SUBMITQUEUE_NEW,
			&req, sizeof(req));
	if (ret) {
		err(ret, "could not create submitqueue");
		return 0;
	}

	return req.id;
}

/**
 * Open kernel device
 */
struct kmd_device *
kmd_dev_open(void)
{
	struct kmd_device *dev;
	int fd;

	/* super cheezy detection of which drm device to open: */
	fd = try_open_device("/dev/dri/renderD128");
	if (fd < 0)
		fd = try_open_device("/dev/dri/renderD129");

	if (fd < 0)
		return NULL;

	dev = calloc(1, sizeof(*dev));
	dev->fd = fd;

	dev->queue_id = open_submitqueue(dev);

	return dev;
}

/**
 * Submit cmdstream to the kernel, returning a fence/timestamp value.
 */
uint32_t
kmd_dev_submit(struct kmd_device *dev, struct kmd_bo *cmds,
		uint32_t sizedwords)
{
	struct drm_msm_gem_submit_cmd req_cmds = {
			.type = MSM_SUBMIT_CMD_BUF,
			.submit_idx = 0,
			.submit_offset = 0,
			.size = sizedwords * 4,
	};
	struct drm_msm_gem_submit req = {
			.flags = MSM_PIPE_3D0 | MSM_SUBMIT_NO_IMPLICIT,
			.queueid = dev->queue_id,
			.bos = VOID2U64(dev->bos),
			req.nr_bos = dev->nr_bos,
			req.cmds = VOID2U64(&req_cmds),
			req.nr_cmds = 1,
	};
	int ret;

	ret = drmCommandWriteRead(dev->fd, DRM_MSM_GEM_SUBMIT,
			&req, sizeof(req));
	if (ret) {
		err(ret, "submit failed");
	}

	return req.fence;
}


static void
get_abs_timeout(struct drm_msm_timespec *tv, uint64_t ns)
{
	struct timespec t;
	uint32_t s = ns / 1000000000;
	clock_gettime(CLOCK_MONOTONIC, &t);
	tv->tv_sec = t.tv_sec + s;
	tv->tv_nsec = t.tv_nsec + ns - (s * 1000000000);
}

/**
 * Wait for fence.
 */
void
kmd_dev_waitfence(struct kmd_device *dev, uint32_t fence)
{
	struct drm_msm_wait_fence req = {
			.fence = fence,
			.queueid = dev->queue_id,
	};
	int ret;

	get_abs_timeout(&req.timeout, 5000000000);

	ret = drmCommandWrite(dev->fd, DRM_MSM_WAIT_FENCE, &req, sizeof(req));
	if (ret) {
		err(ret, "wait-fence failed");
	}
}

/**
 * Allocate a GPU buffer object with specified size (in bytes).
 */
struct kmd_bo *
kmd_bo_alloc(struct kmd_device *dev, int size)
{
	struct kmd_bo *bo;
	struct drm_msm_gem_new req = {
			.size = size,
			.flags = MSM_BO_WC,
	};
	int ret, idx;

	ret = drmCommandWriteRead(dev->fd, DRM_MSM_GEM_NEW,
			&req, sizeof(req));
	if (ret) {
		err(ret, "buffer allocation failed");
		return NULL;
	}

	bo = calloc(1, sizeof(*bo));
	bo->dev = dev;
	bo->handle = req.handle;
	bo->size = size;

	idx = dev->nr_bos++;

	dev->bos[idx].flags = MSM_SUBMIT_BO_READ | MSM_SUBMIT_BO_WRITE;
	dev->bos[idx].handle = bo->handle;
	dev->bos[idx].presumed = kmd_bo_iova(bo);

	return bo;
}

/**
 * Map the specified buffer for CPU access.  The buffer should be
 * mapped uncached/writecombine.
 */
void *
kmd_bo_map(struct kmd_bo *bo)
{
	struct drm_msm_gem_info req = {
			.handle = bo->handle,
			.info = MSM_INFO_GET_OFFSET,
	};
	void *ptr;
	int ret;

	/* if the buffer is already backed by pages then this
	 * doesn't actually do anything (other than giving us
	 * the offset)
	 */
	ret = drmCommandWriteRead(bo->dev->fd, DRM_MSM_GEM_INFO,
			&req, sizeof(req));
	if (ret) {
		err(ret, "alloc failed");
		return NULL;
	}

	ptr = os_mmap(0, bo->size, PROT_READ | PROT_WRITE, MAP_SHARED,
			bo->dev->fd, req.value);
	if (ptr == MAP_FAILED) {
		err(-1, "mmap failed");
	}

	return ptr;
}

/**
 * Get the buffers GPU iova
 */
uint64_t
kmd_bo_iova(struct kmd_bo *bo)
{
	struct drm_msm_gem_info req = {
			.handle = bo->handle,
			.info = MSM_INFO_GET_IOVA,
	};
	int ret;

	if (bo->iova)
		return bo->iova;

	ret = drmCommandWriteRead(bo->dev->fd, DRM_MSM_GEM_INFO, &req, sizeof(req));
	if (ret) {
		err(ret, "could not get iova");
	}

	return req.value;
}
