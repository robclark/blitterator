PKGCONFIG ?= pkg-config
CC     = $(CROSS)gcc -std=c11
CFLAGS = -Wall `$(PKGCONFIG) --cflags libdrm` -I$(SYSROOT)/usr/include
LFLAGS = `$(PKGCONFIG) --libs libdrm`

%.o: %.c *.h
	$(CC) $(CFLAGS) -g -c $< -o $@

blitterator: blitterator.o kmd_drm.o
	$(CC) $^ $(LFLAGS) -o $@

clean:
	rm -f *.o blitterator
